export const environment = {
  production: true,
  BACKEND_URL: 'https://ocv-backend-staging.herokuapp.com',
  ACTIVATED_LOGS: true,
  APP_NAME: 'online-cv-staging'
};
