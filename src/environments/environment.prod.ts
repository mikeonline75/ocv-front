export const environment = {
  production: true,
  BACKEND_URL: 'https://ocv-backend.herokuapp.com',
  ACTIVATED_LOGS: false,
  APP_NAME: 'online-cv'
};
