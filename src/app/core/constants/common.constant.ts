export const APP_CREDITS = {
  copyright: 'Copyright © 2018-2022',
  owner: 'MIKEONLINE',
  fullName: 'Mickaël NODANCHE',
  title: 'Développeur FullStack TS CV en ligne',
  subTitle: 'Angular 2+, Nest.js, Node.js',
  webAppUrl: 'https://cv-mikeonline.web.app'
};

export const EXPERTISE_LEVEL_LABEL: { [key: string]: string } = {
  '1': 'débutant',
  '2': 'averti',
  '3': 'bon',
  '4': 'très bon',
  '5': 'expert',
};

export const PRIORITY_LABEL: { [key: string]: string } = {
  '1': 'au besoin',
  '2': 'normal',
  '3': 'maximum',
};
