export type MomentDateComparedResult = (
  'isSame' | 'isBefore' | 'isAfter'
);
