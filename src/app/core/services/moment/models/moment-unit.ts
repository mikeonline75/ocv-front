export type MomentUnit = (
  'year' | 'years' | 'y' |
  'month' | 'months' | 'M' |
  'week' | 'weeks' | 'w' |
  'day' | 'days' | 'd' |
  'hour' | 'hours' | 'h' |
  'minute' | 'minutes' | 'm' |
  'second' | 'seconds' | 's' |
  'millisecond' | 'milliseconds' | 'ms'
);

export enum MomentUnits {
  YEARS = 'y',
  QUARTERS = 'Q',
  MONTHS = 'M',
  WEEKS = 'w',
  DAYS = 'd',
  HOURS = 'h',
  MINUTES = 'm',
  SECONDS = 's',
  MILLISECONDS = 'ms'
}
