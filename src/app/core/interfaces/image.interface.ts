export interface IImage {
  src: string;
  alt?: string;
  title?: string;
  width?: number;
  height?: number;
}
