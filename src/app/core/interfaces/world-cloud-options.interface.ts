export interface WorldCloudOptions {
  gridSize: number;

  fontFamily: string;

  color: () => string;

  backgroundColor: string;

  rotateRatio: number;

  classes: string;
}
