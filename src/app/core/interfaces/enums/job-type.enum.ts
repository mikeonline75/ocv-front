export enum EJobType {
  Freelance,
  Founder,
  CDI,
  CDD,
  Interim,
  Stage,
  Alternance
}
