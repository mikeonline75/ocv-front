export enum ETagCategory {
  Frontend,
  Backend,
  Software,
  Cloud,
  Dev,
  Ops,
  Project
}
