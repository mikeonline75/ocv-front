export interface ILink {
  title: string;
  url: string;
  description?: string;
}
