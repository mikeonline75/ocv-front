export interface IWebLink {
  name: string;
  icon?: string;
  image?: { src: string; alt: string; height: string; width: string; };
  description: string;
  url: string;
}
