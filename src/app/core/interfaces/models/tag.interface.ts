export interface ITag {
  id: number;
  name: string;
  weight: number;
  times?: number;
}
