export interface IInterest {
  title: string;
  description: string;
  link: {
    url: string;
    title: string;
  };
}
