export interface IDescription {
  short: string;
  long: string;
}
