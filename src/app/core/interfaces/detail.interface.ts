export interface IDetail {
  title?: string;
  url?: string;
  lineList?: string[];
  date?: string;
}
