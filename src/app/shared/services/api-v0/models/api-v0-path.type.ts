export type ApiV0Path = 'cv-infos' |
  'tags' |
  'competences' |
  'experiences' |
  'formations' |
  'projects' |
  'interests';
